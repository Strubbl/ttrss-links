package main

import (
	"fmt"
	"os"
)

func main() {
	err := ReadConfig(configFileName)
	if err != nil {
		fmt.Println(err)
		fmt.Println("cannot open", configFileName, "but going to create a new dummy config file. please adjust its values and re-run")
		writeError := WriteConfig()
		if writeError != nil {
			panic(writeError)
		}
		os.Exit(1)
	}
	if Config.SessionID == "" {
		Config.SessionID, err = getSessionID()
		if err != nil {
			panic(err)
		}
	}

	apiLvl, err := getAPILevel()
	if err != nil {
		// could not retrieve tt-rss version, so all following calls would fail, too
		panic(err)
	}
	if apiLvl == 0 {
		panic(fmt.Errorf("reported API level is 0"))
	}

	starredLinks, err := getStarredLinks(0)
	if err != nil {
		panic(err)
	} else {
		fmt.Printf(starredLinks)
	}
}
