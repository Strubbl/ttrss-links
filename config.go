package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
)

const configFileName = "config.json"

var Config TtrssConfig

type TtrssConfig struct {
	URL       string
	HtUser    string
	HtPass    string
	User      string
	Pass      string
	SessionID string
}

// NewTtrssConfig initializes a new TtrssConfig with the given values
func NewTtrssConfig(url, htuser, htpass, user, pass, sessionid string) TtrssConfig {
	return TtrssConfig{url, htuser, htpass, user, pass, sessionid}
}

// ReadConfig will read the configuration from the given configJSON
// file and set the global Config setting with the results of the
// parsing
func ReadConfig(configJSON string) (err error) {
	Config, err = getConfig(configJSON)
	return
}

func WriteConfig() (err error) {
	if Config.URL == "" {
		Config = NewTtrssConfig("http://example.com/tt-rss/api/", "", "", "admin", "555nose", "")
	}
	data, err := json.MarshalIndent(Config, "", "  ")
	if err != nil {
		return err
	}
	ioutil.WriteFile(configFileName, data, 0666)
	return
}

// getConfig reads a given configJSON file and parses the result, returning a parsed config object
func getConfig(configJSON string) (config TtrssConfig, err error) {
	raw, err := ioutil.ReadFile(configJSON)
	if err != nil {
		return
	}
	config, err = readJSON(raw)
	return
}

// readJSON parses a byte stream into a WallabagConfig object
func readJSON(raw []byte) (config TtrssConfig, err error) {
	// trim BOM bytes that make the JSON parser crash
	raw = bytes.TrimPrefix(raw, []byte("\xef\xbb\xbf"))
	err = json.Unmarshal(raw, &config)
	return
}
