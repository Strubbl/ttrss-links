package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

const ( // iota is reset to 0
	apiStatusOk    = iota // API_STATUS_OK == 0
	apiStatusError = iota // API_STATUS_ERR == 1
)

const apiErrorMessage = "API reports status API_STATUS_ERR\nfor more info see https://git.tt-rss.org/git/tt-rss/wiki/ApiReference#150-and-above"
const headlineLimit = 100

// OperationResponse represents the json response to the API operation
type OperationResponse struct {
	Seq     int              `json:"seq"`
	Status  int              `json:"status"`
	Content OperationContent `json:"content"`
}

// OperationContent is part of every OperationContent from the API, not all defined fields are always in it
type OperationContent struct {
	SessionID string `json:"session_id"`
	APILevel  int    `json:"api_level"` // is returned on op=login
	Level     int    `json:"level"`     // gets returned by op=getApiLevel
	Version   string `json:"version"`
	Error     string `json:"error"`
}

// HeadlineResponse represents the json response to the API operation
type HeadlineResponse struct {
	Seq     int               `json:"seq"`
	Status  int               `json:"status"`
	Content []HeadlineContent `json:"content"`
}

// HeadlineContent is part of every HeadlineResponse from the API
type HeadlineContent struct {
	ID   int    `json:"id"`
	Link string `json:"link"`
}

func headlineUnmarshal(headlineResponseJSON []byte) (hResp HeadlineResponse, err error) {
	err = json.Unmarshal(headlineResponseJSON, &hResp)
	if err != nil {
		return
	}
	if hResp.Status != apiStatusOk {
		return hResp, fmt.Errorf(apiErrorMessage)
	}
	return
}

func genericUnmarshal(genericResponse []byte) (opResp OperationResponse, err error) {
	err = json.Unmarshal(genericResponse, &opResp)
	if err != nil {
		return
	}
	if opResp.Status != apiStatusOk && opResp.Content.Error == "" {
		return opResp, fmt.Errorf(apiErrorMessage)
	}
	if opResp.Content.Error != "" {
		return opResp, fmt.Errorf("API responds with error: %v\nfor more info see https://git.tt-rss.org/git/tt-rss/wiki/ApiReference#testing-api-calls-using-curl", opResp.Content.Error)
	}
	return
}

func apiCall(apiRequest string) (respJSON []byte, err error) {
	//fmt.Println("apiCall: apiRequest =", apiRequest)
	body := strings.NewReader(apiRequest)
	req, err := http.NewRequest("POST", Config.URL, body)
	if err != nil {
		return
	}
	// http basic auth
	if Config.HtUser != "" {
		req.SetBasicAuth(Config.HtUser, Config.HtPass)
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("API response status is %v", resp.StatusCode)
	}
	respJSON, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}
	//fmt.Println("apiCall: respJSON =", string(respJSON))
	return
}

func getSessionID() (sessID string, err error) {
	// https://git.tt-rss.org/git/tt-rss/wiki/ApiReference#login
	opRespJSON, err := apiCall(strings.Join([]string{`{"op":"login","user":"`, Config.User, `","password":"`, Config.Pass, `"}`}, ""))
	if err != nil {
		return
	}
	opResp, err := genericUnmarshal(opRespJSON)
	if err != nil {
		return
	}
	sessID = opResp.Content.SessionID
	return
}

func getTtrssVersion() (ttrssVersion string, err error) {
	// https://git.tt-rss.org/git/tt-rss/wiki/ApiReference#getversion
	opRespJSON, err := apiCall(strings.Join([]string{`{"sid":"asdf`, Config.SessionID, `","op":"getVersion"}`}, ""))
	if err != nil {
		return
	}
	opResp, err := genericUnmarshal(opRespJSON)
	if err != nil {
		return
	}
	ttrssVersion = opResp.Content.Version
	return
}

func getAPILevel() (apiLevel int, err error) {
	// https://git.tt-rss.org/git/tt-rss/wiki/ApiReference#getapilevel-since-version158-api-level-1
	opRespJSON, err := apiCall(strings.Join([]string{`{"sid":"`, Config.SessionID, `","op":"getApiLevel"}`}, ""))
	if err != nil {
		return
	}
	opResp, err := genericUnmarshal(opRespJSON)
	if err != nil {
		return
	}
	apiLevel = opResp.Content.Level
	return
}

func getStarredLinks(skip int) (links string, err error) {
	// https://git.tt-rss.org/git/tt-rss/wiki/ApiReference#getheadlines
	//Special feed IDs are as follows:
	//    -1 starred
	//    -2 published
	//    -3 fresh
	//    -4 all articles
	//    0 - archived
	//    IDs < -10 labels

	opRespJSON, err := apiCall(strings.Join([]string{`{"sid":"`, Config.SessionID, `","op":"getHeadlines","feed_id":-1,"limit":`, strconv.Itoa(headlineLimit), `,"skip":`, strconv.Itoa(skip), `}`}, ""))
	if err != nil {
		return
	}
	opResp, err := headlineUnmarshal(opRespJSON)
	if err != nil {
		// TODO do not return and instead try generic unmarshal for better error message
		return
	}
	for _, headline := range opResp.Content {
		if headline.Link != "" {
			fmt.Println(headline.Link)
		}
	}
	// API call getHeadlines returns empty array when there are no more headlines
	if len(opResp.Content) > 0 {
		getStarredLinks(skip + headlineLimit)
	}
	return
}
